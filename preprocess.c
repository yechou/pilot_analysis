#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TNtuple.h>
#include<TLeaf.h>
#include<stdlib.h>
#include<iostream>
void preprocess(){
	TFile f("linked_tracks.root");
	TTree *t = (TTree*)f.Get("tracks");
	TBranch *b = t->FindBranch("s");
	TLeaf *xleaf = t->FindLeaf("eX");
	TLeaf *yleaf = t->FindLeaf("eY");
	TLeaf *zleaf = t->FindLeaf("eZ");
	TLeaf *txleaf = t->FindLeaf("eTX");
	TLeaf *tyleaf = t->FindLeaf("eTY");
	TLeaf *izleaf = t->FindLeaf("s.eScanID.ePlate");
	TLeaf *idleaf = t->FindLeaf("eID");
	TFile g("straighttracks.root","RECREATE");
	TNtuple *nt = new TNtuple("ntuple","","id:x:y:z:tx:ty:iz:nbts");
	int nentries = b->GetEntries();
	for(int i=0;i<nentries;i++){
		b->GetEntry(i);
		int nbts = xleaf->GetLen();
		for(int j=0;j<nbts;j++){
			if(i%100000==0)std::cout << i << std::endl;
			double x = xleaf->GetValue(j);
			double y = yleaf->GetValue(j);
			double z = zleaf->GetValue(j);
			double tx = txleaf->GetValue(j);
			double ty = tyleaf->GetValue(j);
			int iz = izleaf->GetValue(j);
			int id = idleaf->GetValue(j);
			//std::cout << x << " " << y << " " << z << " " << tx << " " << ty << " " << iz << " " << id << " " << llen << std::endl;
			nt->Fill(id,x,y,z,tx,ty,iz,nbts);
		}
	}
	nt->Write();
	g.Close();
}